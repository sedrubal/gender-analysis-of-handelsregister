# Gender Analysis on handelsregister

This scripts are built to analyze the gender distribution of leading positions of persons in
companies registered to handelsregister.
It uses the great work of [openregister](https://offeneregister.de/).

## Run it

```bash
$ poetry install
$ poetry shell
$ # download & extract the openregister database (SQLite)
$ # (use torrent if possible. See https://offeneregister.de/#download )
$ # estimate genders of names and wait (very long)
$ ./determine_genders.py
$ # do the evaluation (will take ~15 minutes)
$ ./evaluate.py
[...]
Estimated Gender Distribution of currently registerd companies:

- in position Prokurist: ♀: 105716 | ♂: 409091 | 21.54 % female
- in position Geschäftsführer: ♀: 388542 | ♂: 1897577 | 17.00 % female
- in position Vorstand: ♀: 368 | ♂: 1748 | 17.39 % female
- in position Liquidator: ♀: 15682 | ♂: 69965 | 18.31 % female
- in position Persönlich haftender Gesellschafter: ♀: 388542 | ♂: 1897577 | 17.00 % female
- in position Inhaber: ♀: 19895 | ♂: 74676 | 21.04 % female
- Total: ♀: 543973 | ♂: 2516726 | 17.77 % female
```

## Disclaimer

The quality of the data is really bad: <https://offeneregister.de/daten/>
This code does not take any countermeasures to improve the quality.
But to determine the gender distribution it might be enough.
The results however are comparable to:
<https://de.statista.com/statistik/daten/studie/182468/umfrage/geschaeftsfuehrer-nach-geschlecht-und-bundeslaendern/>

## License

(c) 2021 - @sedrubal - [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
