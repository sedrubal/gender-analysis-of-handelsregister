#!/usr/bin/env python3
"""Do some evaluation. Run determine_genders.py before."""

import argparse
import sqlite3
import typing
from functools import cached_property, lru_cache

from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import HTML
from prompt_toolkit.shortcuts import ProgressBar

# we would have to know the probability distribution of the n most common first names
GENDER_CACHE_MAX_SIZE = 20000

SELECT_REGISTERED_COMPANY_NUMBER_QUERY = (
    'SELECT company_number FROM company WHERE current_status="currently registered";'
)
SELECT_FIRSTNAME_QUERY = (
    "SELECT firstname FROM officer WHERE position=? AND type='person' AND company_id=?;"
)
SELECT_GENDER_QUERY = "SELECT gender from names WHERE firstname=? LIMIT 1;"
SELECT_KNOWN_POSITIONS = "SELECT DISTINCT(position) FROM officer;"

Gender = typing.Union[
    typing.Literal["female"],
    typing.Literal["male"],
    typing.Literal["?"],
]


def parse_args():
    """Parse command line args"""
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument(
        "-o",
        "--openregister",
        dest="openregister_file",
        action="store",
        default="openregister.db",
        help="The openregister sqlite database.",
    )
    parser.add_argument(
        "-r",
        "--results",
        dest="results_file",
        action="store",
        default="results.db",
        help="The sqlite database to write ethe results.",
    )

    return parser.parse_args()


class Evaluator:
    """Do some evaluations."""

    def __init__(
        self,
        openregister_file: str = "./openregister.db",
        results_file: str = "./results.db",
    ):
        self.openregister_db = sqlite3.connect(openregister_file)
        self.results_db = sqlite3.connect(results_file)
        self.total_registered_companies = 0

    @cached_property
    def registered_company_ids(self) -> typing.FrozenSet[str]:
        """Return a list of currently registered company IDs."""
        cursor = self.openregister_db.execute(SELECT_REGISTERED_COMPANY_NUMBER_QUERY)
        companies = set()

        with ProgressBar() as prog_bar:
            for row in prog_bar(cursor, label=HTML("<bold>Fetching registered companies.</bold>")):
                companies.add(row[0])

        return frozenset(companies)

    def _get_relating_persons(self, company_id: str, position: str) -> typing.List[str]:
        """Get the relating persons of a company mathing ``position``."""
        cursor = self.openregister_db.execute(SELECT_FIRSTNAME_QUERY, (position, company_id))

        return [row[0] for row in cursor]

    @lru_cache(maxsize=GENDER_CACHE_MAX_SIZE)
    def _get_gender(self, firstname: str) -> Gender:
        """Get the gender for ``fistname``."""
        cursor = self.results_db.execute(SELECT_GENDER_QUERY, (firstname,))
        row = cursor.fetchone()

        if row is None:
            return "?"

        gender = row[0]
        assert gender in {"female", "male", "?"}

        return gender or "?"

    def count_genders(self, position: str) -> typing.Tuple[int, int]:
        """Count genders of persons in ``position`` of a currently registered company."""

        title = HTML(f"<bold>Counting genders for type <magenta>{position}</magenta></bold>")

        gender_counter = {
            "female": 0,
            "male": 0,
            "?": 0,
            # don't know, why this happens:
            "companies_without_person": 0,
        }

        def label():
            num_female = gender_counter["female"]
            num_male = gender_counter["male"]
            num_unknown = gender_counter["?"]
            num_total = num_female + num_male + num_unknown
            num_comp_wo_person = gender_counter["companies_without_person"]

            return HTML(
                " | ".join(
                    (
                        f"<yellow>♀: {num_female}</yellow>",
                        f"<cyan>♂: {num_male}</cyan>",
                        f"∑ {num_total}",
                        f"<red>comp. w/o {position}: {num_comp_wo_person}</red>",
                    )
                )
            )

        with ProgressBar(title=title) as prog_bar:
            for company_id in prog_bar(
                self.registered_company_ids,
                label=label,
                total=self.total_registered_companies,
            ):
                persons = self._get_relating_persons(company_id, position)

                if not persons:
                    gender_counter["companies_without_person"] += 1
                else:
                    for person in persons:
                        gender = self._get_gender(person)
                        gender_counter[gender] += 1

        return gender_counter["female"], gender_counter["male"]

    @cached_property
    def known_positions(self) -> typing.FrozenSet[str]:
        """Return the know ``positions``."""
        cursor = self.openregister_db.execute(SELECT_KNOWN_POSITIONS)

        return frozenset(row[0] for row in cursor)

    def _print_cache_stats(self):
        """Print some stats about _get_gender cache."""
        cache_info = self._get_gender.cache_info()
        print_formatted_text(
            HTML(
                f"Cache info for gender lookups "
                f"<grey>(to optimize GENDER_CACHE_MAX_SIZE)</grey>: "
                f"<green>Cache Hits: {cache_info.hits}</green> | "
                f"<red>Cache Misses: {cache_info.misses}</red> | "
                f"<grey>Cache Fill Level: {cache_info.currsize * 100 / cache_info.maxsize:.2f} %</grey>"
            )
        )

    def run(self):
        """Do the job."""
        self.total_registered_companies = len(self.registered_company_ids)

        results = []

        for position in self.known_positions:
            num_female, num_male = self.count_genders(position=position)
            results.append(
                (position, num_female, num_male)
            )
            self._print_cache_stats()

        print()
        print_formatted_text(
            HTML(
                "<bold>Estimated Gender Distribution of currently registerd companies:</bold> "
            )
        )
        print()

        sum_num_female = 0
        sum_num_male = 0
        for position, num_female, num_male in results:
            percent_female = num_female * 100 / ((num_female + num_male) or 1)
            sum_num_female += num_female
            sum_num_male += num_male
            print_formatted_text(
                HTML(
                    f"<bold>- in position <magenta>{position}</magenta>:</bold> "
                    f"<yellow>♀: {num_female}</yellow> | "
                    f"<cyan>♂: {num_male}</cyan> | "
                    f"<orange>{percent_female:.2f} % female</orange>"
                ),
            )

        total_percent_female = sum_num_female * 100 / ((sum_num_female + sum_num_male) or 1)

        print_formatted_text(
            HTML(
                f"<bold>"
                f"- Total: "
                f"<yellow>♀: {sum_num_female}</yellow> | "
                f"<cyan>♂: {sum_num_male}</cyan> | "
                f"<orange>{total_percent_female:.2f} % female</orange>"
                f"</bold> "
            ),
        )

        self.openregister_db.close()
        self.results_db.close()


def main():
    """Run the tool."""
    args = parse_args()
    evaluator = Evaluator(**dict(args._get_kwargs()))  # pylint: disable=W0212
    try:
        evaluator.run()
    except KeyboardInterrupt:
        print()
        print_formatted_text(HTML("<red>quit</red>"))


if __name__ == "__main__":
    main()
