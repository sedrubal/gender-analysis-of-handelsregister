#!/usr/bin/env python3
"""Determine the gender for all first names."""

# pylint: disable=C0415  import-outside-toplevel
# pylint: disable=E0401  import-error
# pylint: disable=R0902  too-many-instance-attributes

import argparse
import sqlite3
import sys
import typing
from enum import Enum
from json import JSONDecodeError

from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import HTML
from prompt_toolkit.shortcuts import ProgressBar

if typing.TYPE_CHECKING:
    import types

    import gender_predictor
    import requests


class GenderizeMethod(Enum):
    """The method to use to determine gender of names."""

    GENDERIZE_IO = "genderize.io"
    GENDER_PREDICTOR = "gender_predictor"

    def __str__(self) -> str:
        return str(self.value)


Gender = typing.Union[typing.Literal["female"], typing.Literal["male"]]


GENDERIZE_API = "https://api.genderize.io/"
GENDERIZE_IO_BULK_SIZE = 200

CREATE_RESULTS_DB_SQL = """
CREATE TABLE IF NOT EXISTS names(
    firstname CHAR(512) PRIMARY KEY NOT NULL,
    gender CHAR(6),
    gender_probability REAL
);
"""

_GET_OPENREGISTER_FIRSTNAMES_BASE_QUERY = (
    "SELECT {select} FROM officer WHERE type='person' AND firstname != '';"
)
SELECT_OPENREGISTER_FIRSTNAMES_SQL = _GET_OPENREGISTER_FIRSTNAMES_BASE_QUERY.format(
    select="DISTINCT(firstname)"
)
GET_OPENREGISTER_FIRSTNAMES_COUNT_SQL = _GET_OPENREGISTER_FIRSTNAMES_BASE_QUERY.format(
    select="COUNT(DISTINCT(firstname))"
)

INSERT_RESULT_FIRSTNAME_SQL = "INSERT INTO names(firstname) VALUES(?);"
INSERT_RESULT_GENDER_SQL = "UPDATE names SET gender=?, gender_probability=? WHERE firstname=?;"
CHECK_RESULT_FIRSTNAME_SQL = "SELECT firstname FROM names WHERE firstname=? LIMIT 1;"
GET_COUNT_OF_NAMES = "SELECT COUNT(*) FROM names;"

_DETERMINATION_BASE_QUERY = "SELECT {select} FROM names WHERE gender IS NULL;"
SELECT_NAMES_UNKNOWN_GENDER_SQL = _DETERMINATION_BASE_QUERY.format(select="firstname")
GET_UNKNOWN_GENDER_COUNT = _DETERMINATION_BASE_QUERY.format(select="COUNT(*)")

_VALIDATION_BASE_QUERY = (
    "SELECT {select} FROM names WHERE gender IS NOT NULL and gender_probability != -1;"
)
GET_GENDERIZE_IO_GENDER_COUNT = _VALIDATION_BASE_QUERY.format(select="COUNT(*)")
SELECT_GENDERIZE_IO_GENDERS_SQL = _VALIDATION_BASE_QUERY.format(
    select="firstname, gender, gender_probability"
)


def parse_args():
    """Parse command line args"""
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument(
        "-m",
        "--genderize-method",
        choices=GenderizeMethod,
        default=GenderizeMethod.GENDER_PREDICTOR,
        type=GenderizeMethod,
        help="The method to determine the gender for names",
    )
    parser.add_argument(
        "-su",
        "--skip-name-update",
        action="store_true",
        help=(
            "Skip updating names from openregister db."
            " Use only the names already inserted in results db."
        ),
    )
    parser.add_argument(
        "-sv",
        "--skip-validation",
        action="store_true",
        help=("Skip validation of genders at end of determination."),
    )
    parser.add_argument(
        "-o",
        "--openregister",
        dest="openregister_file",
        action="store",
        default="openregister.db",
        help="The openregister sqlite database.",
    )
    parser.add_argument(
        "-r",
        "--results",
        dest="results_file",
        action="store",
        default="results.db",
        help="The sqlite database to write ethe results.",
    )

    return parser.parse_args()


class NameAnalyzer:
    """Analyze the dataset."""

    def __init__(
        self,
        genderize_method: GenderizeMethod = GenderizeMethod.GENDER_PREDICTOR,
        skip_name_update: bool = False,
        skip_validation: bool = False,
        openregister_file: str = "./openregister.db",
        results_file: str = "./results.db",
    ):
        self.genderize_method = genderize_method
        self.skip_name_update = skip_name_update
        self.skip_validation = skip_validation

        if genderize_method == GenderizeMethod.GENDERIZE_IO:
            import requests

            self.requests: typing.Optional["types.ModuleType"] = requests
            self.session: typing.Optional["requests.Session"] = requests.Session()
            self.gender_predictor: typing.Optional["gender_predictor.GenderPredictor"] = None
        elif genderize_method == GenderizeMethod.GENDER_PREDICTOR:
            import gender_predictor

            self.requests = None
            self.session = None
            self.gender_predictor = gender_predictor.GenderPredictor()
            self.gender_predictor.train_and_test()
        else:
            raise AssertionError
        self.openregister_db = sqlite3.connect(openregister_file)
        self.results_db = sqlite3.connect(results_file)
        self._genderize_io_queue: typing.List[str] = []
        self._stats = {
            "processed": 0,
            "genderize_api_calls": 0,
            "person_count": 0,
            "predict_gender": 0,
            "name_already_exists": 0,
            "new_names": 0,
            "names_in_openregister": 0,
            # for validation:
            "updated_genders": 0,
            "missmatching_genders": 0,
            "matching_genders": 0,
        }

    def _check_if_name_exists(self, firstname: str) -> bool:
        """Check if firstname is already inserted in results db."""
        cursor = self.results_db.execute(CHECK_RESULT_FIRSTNAME_SQL, (firstname,))
        result = cursor.fetchone()

        if result is None:
            return False

        (name,) = result
        assert name == firstname

        return True

    def _insert_firstname(self, firstname: str):
        """Insert firstname into results db."""
        self.results_db.execute(INSERT_RESULT_FIRSTNAME_SQL, (firstname,))
        self.results_db.commit()

    def update_names(self):
        """Copy all firstnames from openregister to result db."""
        total = self._get_openregister_name_count()
        self._stats["names_in_openregister"] = total
        cursor = self.openregister_db.execute(SELECT_OPENREGISTER_FIRSTNAMES_SQL)

        title = HTML("<bold>Updating firstnames in results db from openregister db...</bold>")

        def label():
            new = self._stats["new_names"]
            existing = self._stats["name_already_exists"]

            return HTML(
                " | ".join(
                    (
                        f"<yellow>new: {new}</yellow>",
                        f"<green>existing: {existing}</green>",
                        f"total {new + existing}",
                    )
                )
            )

        with ProgressBar(title=title) as prog_bar:
            for row in prog_bar(cursor, label=label, total=total):
                (firstname,) = row

                if self._check_if_name_exists(firstname):
                    self._stats["name_already_exists"] += 1
                else:
                    self._insert_firstname(firstname)
                    self._stats["new_names"] += 1

    @staticmethod
    def _get_amount_query(query: str, con: sqlite3.Connection) -> int:
        """:Return the value of a COUNT(*) query."""
        cursor = con.execute(query)
        result = cursor.fetchone()
        (amount,) = result

        return int(amount)

    def _get_openregister_name_count(self):
        """:Return: The amount of distinct firstnames in openregister."""

        return self._get_amount_query(
            GET_OPENREGISTER_FIRSTNAMES_COUNT_SQL, con=self.openregister_db
        )

    def _get_name_count(self):
        """:Return: The amount of firstnames."""

        return self._get_amount_query(GET_COUNT_OF_NAMES, con=self.results_db)

    def _get_unknown_gender_count(self):
        """:Return: The amount of firstnames."""

        return self._get_amount_query(GET_UNKNOWN_GENDER_COUNT, con=self.results_db)

    def _get_genderize_io_gender_count(self):
        """:Return: The amount of genders determined using genderize.io."""

        return self._get_amount_query(GET_GENDERIZE_IO_GENDER_COUNT, con=self.results_db)

    def _fetch_gender(self, firstname: str):
        """
        Schedule task to get gender.

        This collects some tasks and makes one request for many names.
        """

        if self.genderize_method == GenderizeMethod.GENDERIZE_IO:
            self._determine_genderize_io(firstname)
        elif self.genderize_method == GenderizeMethod.GENDER_PREDICTOR:
            self._determine_gender_predictor(firstname)
        else:
            assert False

    def _save_gender(
        self,
        firstname: str,
        gender: typing.Union[typing.Optional[Gender], typing.Literal["?"]],
        gender_probability: float,
        commit: bool = True,
    ):
        """Save the gender for an existing firstname to results db."""
        self.results_db.execute(INSERT_RESULT_GENDER_SQL, (gender, gender_probability, firstname))

        if commit:
            self.results_db.commit()

    def _get_gender_from_gender_predictor(self, firstname: str) -> typing.Optional[Gender]:
        """Return the gender determined by gender_predictor."""
        assert self.gender_predictor
        gender = self.gender_predictor.classify(firstname)
        gender = "female" if gender == "F" else "male" if gender == "M" else None

        return gender

    def _determine_gender_predictor(self, firstname: str):
        """Determine gender using gender_predictor."""
        gender = self._get_gender_from_gender_predictor(firstname)
        self._save_gender(firstname, gender, gender_probability=-1)
        self._stats["predict_gender"] += 1

    def _determine_genderize_io(self, firstname):
        """Process scheduled tasks using genderize.io"""
        self._genderize_io_queue.append(firstname)

        if len(self._genderize_io_queue) >= GENDERIZE_IO_BULK_SIZE:
            self._process_genderize_io_queue()

    def _process_genderize_io_queue(self):
        """Process all scheduled firstnames that should be fetched from genderize.io."""

        if self.genderize_method != GenderizeMethod.GENDERIZE_IO or not self._genderize_io_queue:
            return

        assert self.requests and self.session

        response = self.session.get(
            GENDERIZE_API,
            params={"name": self._genderize_io_queue, "country_id": "DE"},
        )
        self._stats["genderize_api_calls"] += 1
        try:
            response.raise_for_status()
        except self.requests.exceptions.HTTPError as err:
            print(file=sys.stderr)
            try:
                error = response.json().get("error", None)
                print(error, file=sys.stderr)

                if error == "Request limit reached":
                    sys.exit(2)
            except JSONDecodeError:
                pass

            print(err, file=sys.stderr)
            sys.exit(1)

        try:
            data = response.json()
        except JSONDecodeError as err:
            print(file=sys.stderr)
            print(err, file=sys.stderr)
            sys.exit(1)

        for entry in data:
            firstname = entry["name"]

            if firstname not in self._genderize_io_queue:
                print(
                    f"Did not request name {entry['name']} but received it from API."
                    f" Requests: {self._genderize_io_queue}",
                    file=sys.stderr,
                )

            self._save_gender(
                firstname=firstname,
                gender=entry["gender"],
                gender_probability=entry["probability"],
                commit=False,
            )
            self._genderize_io_queue.remove(firstname)

        self.results_db.commit()

    def determine_genders(self):
        """Determine gender of all firstnames with unknown gender."""
        cursor = self.results_db.execute(SELECT_NAMES_UNKNOWN_GENDER_SQL)
        title = HTML("<bold>Determining Genders</bold>")
        total = self._stats["unkown_gender"]
        already_determined = self._stats["person_count"] - total

        def label():
            processed = self._stats["processed"]

            if self.genderize_method == GenderizeMethod.GENDERIZE_IO:
                determinations = self._stats["genderize_api_calls"]
                determination_str = "genderize.io API calls"
            else:
                determinations = self._stats["predict_gender"]
                determination_str = "Gender Predictions"

            return HTML(
                " | ".join(
                    (
                        f"<yellow>Processed {processed}</yellow>",
                        f"<blue>{determination_str} {determinations}</blue>",
                        f"already determined before {already_determined}",
                    )
                )
            )

        with ProgressBar(title=title) as prog_bar:
            for row in prog_bar(cursor, label=label, total=total):
                (firstname,) = row
                self._fetch_gender(firstname)
                self._stats["processed"] += 1

        self._process_genderize_io_queue()

    def create_results_db(self):
        """
        Create db tables of results db.
        """
        self.results_db.execute(CREATE_RESULTS_DB_SQL)

    def validate(self):
        """Validate genders determined with genderize.io against gender_predictor."""
        cursor = self.results_db.execute(SELECT_GENDERIZE_IO_GENDERS_SQL)
        total = self._get_genderize_io_gender_count()
        differences: typing.Set[typing.Tuple[str, str, str]] = set()

        title = HTML("<bold>Validating genderize.io genders agains gender_predictor.</bold>")

        def label():
            updated_genders = self._stats["updated_genders"]
            missmatching_genders = self._stats["missmatching_genders"]
            matching_genders = self._stats["matching_genders"]

            return HTML(
                " | ".join(
                    (
                        f"<yellow>Updated {updated_genders}</yellow>",
                        f"<red>Missmatching {missmatching_genders}</red>",
                        f"<green>Matching {matching_genders}</green>",
                    )
                )
            )

        with ProgressBar(title=title) as prog_bar:
            for row in prog_bar(cursor, label=label, total=total):
                (firstname, gender_genderize_io, gender_probability) = row
                gender_gp = self._get_gender_from_gender_predictor(firstname)

                if gender_gp and gender_gp != gender_genderize_io:
                    if gender_probability <= 0.5:
                        self._save_gender(
                            firstname=firstname,
                            gender=gender_gp,
                            gender_probability=-1,
                            commit=True,
                        )
                        self._stats["updated_genders"] += 1
                    else:
                        differences.add(
                            (
                                firstname,
                                gender_genderize_io,
                                gender_gp,
                            )
                        )
                        self._save_gender(
                            firstname=firstname,
                            gender="?",
                            gender_probability=0,
                            commit=True,
                        )
                        self._stats["missmatching_genders"] += 1
                else:
                    self._stats["matching_genders"] += 1

        if differences:
            print_formatted_text(
                HTML("<bold>Could not determine gender for following names</bold>")
            )
            print("\tFirstname\tGenderize.io\tGender Predictor")

            for firstname, gender_genderize_io, gender_gp in differences:
                print(f"- {firstname}\t{gender_genderize_io}\t{gender_gp}")
        else:
            print_formatted_text(HTML("<green>All genders are ok.</green>"))

    def run(self):
        """Run the analyzer."""
        try:
            self.create_results_db()

            if not self.skip_name_update:
                self.update_names()
            else:
                print("Skipping name update")

            self.openregister_db.close()
            self._stats["person_count"] = self._get_name_count()
            self._stats["unkown_gender"] = self._get_unknown_gender_count()
            self.determine_genders()

            if not self.skip_validation:
                self.validate()
            else:
                print("Skipping validation")
        except KeyboardInterrupt:
            print()
            print("Abort")


def main():
    """Run the tool."""
    args = parse_args()
    analyzer = NameAnalyzer(**dict(args._get_kwargs()))  # pylint: disable=W0212
    analyzer.run()


if __name__ == "__main__":
    main()
